CREATE SCHEMA BancoJava;
USE BancoJava;
CREATE TABLE Usuario(
	id int not null primary key,
	nome varchar(255) not null,
	senha varchar(255) not null
);
CREATE TABLE Texto(
	id int not null primary key,
	titulo varchar(255),
	ano int,
	sinopse varchar(255)
);