package Controle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Modelo.Usuario;

/**
 * Servlet implementation class AlterarSenha
 */
@WebServlet("/AlterarSenha")
public class AlterarSenha extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AlterarSenha() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String senha = request.getParameter("senha");
		String senhaconf = request.getParameter("senhaconf");
		if(senha == senhaconf) {
			Usuario usr = new Usuario();
			usr.setSenha(senha);
			boolean add = new ControleUsuario().AlterarSenha(usr);
			if(add) {
				response.getWriter().print(""
						+ "<script>"
						+ "alert('Senha alterada com sucesso!');"
						+ "window.location.href='livraria.xhtml'"
						+ "</script>");
			}else {
				response.getWriter().print(""
						+ "<script>"
						+ "alert('Erro ao alterar a senha!');"
						+ "window.location.href='livraria.xhtml'"
						+ "</script>");
			}
		}else {
				response.getWriter().print(""
						+ "<script>"
						+ "alert('As senhas n�o correspondem');"
						+ "window.location.href='livraria.xhtml'"
						+ "</script>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String senha = request.getParameter("senha");
		String senhaconf = request.getParameter("senhaconf");
		if(senha == senhaconf) {
			Usuario usr = new Usuario();
			usr.setSenha(senha);
			boolean add = new ControleUsuario().AlterarSenha(usr);
			if(add) {
				response.getWriter().print(""
						+ "<script>"
						+ "alert('Senha alterada com sucesso!');"
						+ "window.location.href='livraria.xhtml'"
						+ "</script>");
			}else {
				response.getWriter().print(""
						+ "<script>"
						+ "alert('Erro ao alterar a senha!');"
						+ "window.location.href='livraria.xhtml'"
						+ "</script>");
			}
		}else{
				response.getWriter().print(""
						+ "<script>"
						+ "alert('As senhas n�o correspondem');"
						+ "window.location.href='livraria.xhtml'"
						+ "</script>");
		}
	}

}
