package Controle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Controle.ControleTexto;
import Modelo.Texto;
/**
 * Servlet implementation class AtualizarTexto
 */
@WebServlet("/AtualizarTexto")
public class AtualizarTexto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AtualizarTexto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String titulo = request.getParameter("titulo");
		String ano = request.getParameter("ano");
		String sinopse = request.getParameter("sinopse");
		Texto txt = new Texto();
		txt.setTitulo(titulo);
		txt.setAno(ano);
		txt.setSinopse(sinopse);
		boolean add = new ControleTexto().AlterarTexto(txt);
		if(add) {
			response.getWriter().print(""
					+ "<script>"
					+ "alert('Conte�do alterado com sucesso!');"
					+ "window.location.href='livrariaAdmin.xhtml'"
					+ "</script>");
		}else {
			response.getWriter().print(""
					+ "<script>"
					+ "alert('Erro ao alterar o conte�do!');"
					+ "window.location.href='livrariaAdmin.xhtml'"
					+ "</script>");
		}
	}

}
