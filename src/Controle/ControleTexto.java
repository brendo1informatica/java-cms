package Controle;

import Modelo.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

//import controle.Conexao;
//import controle.Exception;
import Modelo.Texto;
//import controle.Exception;
//import controle.String;



import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
/**
 * @author guil
 *
 */
// inserir ok
	public class ControleTexto {
	public boolean adicionarUsuario(Texto text, DataSource ds) throws SQLException, Exception {
		boolean resultado = false;
		Connection con = ds.getConnection();
		PreparedStatement ps = con.prepareStatement("INSERT INTO Texto(titulo,ano,sinopse) VALUES(?,?,?)");
		ps.setString(1, text.getTitulo());
		ps.setInt(2, text.getAno());
		ps.setString(3, text.getSinopse());
		if(!ps.execute()) {
			resultado = true;
		}else {
			throw new SQLException("Erro na modificação.");
		}
		return resultado;
	}
// deletar ok
	public boolean deletarUsuario(int id, DataSource ds) {
		boolean resul=false;
		try {
			Connection con = ds.getConnection();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Texto WHERE id=?");
			ps.setInt(1, id);
			if(!ps.execute()) {
				resul=true;
			}
			}catch(SQLException e) {
				e.getMessage();
			}
		return resul;
	}
	// mostrar a tabela ok
	public ArrayList<Texto> allTexto(DataSource ds, int id) throws SQLException, Exception{
		ArrayList<Texto> lista = null;
		Connection con = ds.getConnection();
		String sql = "select a.* from Texto a inner join Texto b on a.id=b.id where b.id=?;";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if(rs != null) {
			lista = new ArrayList<Texto>();
			while(rs.next()){
				Texto text = new Texto();
				text.setTitulo(rs.getString("titulo"));
				text.setAno(rs.getInt("ano"));
				text.setSinopse(rs.getString("sinopse"));
				text.setId(rs.getInt("id"));
				lista.add(text);
			}
		}
		con.close();
		return lista;
	}
	/*public boolean atualizar(Usuario user , DataSource ds){
		boolean resu = false;
		try {
			Connection con = ds.getConnection();
			String sql = "UPDATE Usuario SET nome=?,senha=? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getNome());
			ps.setString(2, user.getSenha());
			if (!ps.execute()) {
				resu = true;
			}
			new Con().fechar(c);
		} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		return resu;*/
	
	}
