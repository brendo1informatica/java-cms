package Controle;

import Modelo.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.sql.DataSource;

//import controle.Conexao;
//import controle.Exception;
import Modelo.Usuario;
//import controle.Exception;
//import controle.String;



import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
/**
 * @author guil
 *
 */
// inserir ok
	public class ControleUsuario {
	public boolean adicionarUsuario(Usuario user, DataSource ds) throws SQLException, Exception {
		boolean resultado = false;
		Connection con = ds.getConnection();
		PreparedStatement ps = con.prepareStatement("INSERT INTO Usuario(nome,senha) VALUES(?,?)");
		ps.setString(1, user.getNome());
		ps.setString(1, user.getSenha());
		if(!ps.execute()) {
			resultado = true;
		}else {
			throw new SQLException("Erro no adicionamento do usu�rio.");
		}
		return resultado;
	}
// deletar ok
	public boolean deletarUsuario(int id, DataSource ds) {
		boolean resul=false;
		try {
			Connection con = ds.getConnection();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Usuario WHERE id=?");
			ps.setInt(1, id);
			if(!ps.execute()) {
				resul=true;
			}
			}catch(SQLException e) {
				e.getMessage();
			}
		return resul;
	}
	// mostrar a tabela ok
	public ArrayList<Usuario> allUsuario(DataSource ds, int id) throws SQLException, Exception{
		ArrayList<Usuario> lista = null;
		Connection con = ds.getConnection();
		String sql = "select a.* from Usuario a inner join Usuario b on a.id=b.id where b.id=?;";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if(rs != null) {
			lista = new ArrayList<Usuario>();
			while(rs.next()){
				Usuario user = new Usuario();
				user.setNome(rs.getString("nome"));
				user.setSenha(rs.getString("senha"));
				user.setId(rs.getInt("id"));
				lista.add(user);
			}
		}
		con.close();
		return lista;
	}
	/*public boolean atualizar(Usuario user , DataSource ds){
		boolean resu = false;
		try {
			Connection con = ds.getConnection();
			String sql = "UPDATE Usuario SET nome=?,senha=? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getNome());
			ps.setString(2, user.getSenha());
			if (!ps.execute()) {
				resu = true;
			}
			new Con().fechar(c);
		} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		return resu;*/
	
	}
